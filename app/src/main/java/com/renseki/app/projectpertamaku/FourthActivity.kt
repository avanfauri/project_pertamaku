package com.renseki.app.projectpertamaku

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.renseki.app.projectpertamaku.model.Mahasiswa

class FourthActivity : AppCompatActivity(), StudentInputFragment.StudentInputActionListener {

    private lateinit var recentStudentFragment: RecentStudentFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fourth_activity)

        tempelFragment1()
        tempelFragment2()
    }

    private fun tempelFragment2() {
        recentStudentFragment = RecentStudentFragment.newInstance()

        supportFragmentManager.beginTransaction().apply {
            replace(
                    R.id.fragment_container_2,
                    recentStudentFragment
            )
            commit()
        }
    }

    private fun tempelFragment1() {
        val fragment = StudentInputFragment.newInstance(this)

        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(
                R.id.fragment_container_1,
                fragment
        )
        fragmentTransaction.commit()
    }

    override fun onMahasiswaReady(mahasiswa: Mahasiswa) {
        Toast
                .makeText(
                        this,
                        mahasiswa.name,
                        Toast.LENGTH_LONG
                )
                .show()
        recentStudentFragment.receiveStudent(mahasiswa)
    }
}
